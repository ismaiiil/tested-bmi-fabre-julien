//
//  CoreLogic.swift
//  BMI2.0
//
//  Created by Ismail Nassurally on 03/12/2018.
//  Copyright © 2018 Julien Fabre. All rights reserved.
//

import Foundation

class CoreLogic {
    func calculate(weight: Double, height: Double) -> Double{
        return weight / (height*height)
        
    }
}
