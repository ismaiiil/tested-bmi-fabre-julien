//
//  calculateViewController.swift
//  BMI2.0
//
//  Created by Ismail Nassurally on 03/12/2018.
//  Copyright © 2018 Julien Fabre. All rights reserved.
//

import UIKit

class calculateViewController: UIViewController {
    var corelogic: CoreLogic?
    @IBOutlet weak var txtweight: UITextField!
    
    @IBOutlet weak var txtheight: UITextField!
    
    @IBAction func btnCalcul(_ sender: Any) {
        corelogic = CoreLogic()
        var bmi = corelogic?.calculate(weight: Double(txtweight.text!)!, height: Double(txtheight.text!)!)
        txtresult.text = String (bmi!)
    }
    
    @IBOutlet weak var txtresult: UITextField!
    
    override func viewDidLoad(){
    super.viewDidLoad()
        
        
    }
    
}
