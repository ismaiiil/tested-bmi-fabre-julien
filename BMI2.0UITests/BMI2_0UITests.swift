//
//  BMI2_0UITests.swift
//  BMI2.0UITests
//
//  Created by Ismail Nassurally on 03/12/2018.
//  Copyright © 2018 Julien Fabre. All rights reserved.
//

import XCTest

class BMI2_0UITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testUserInputsHeightZero(){
        
        let app = XCUIApplication()
        app.buttons["Let's Go"].tap()
        app.children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .textField).element(boundBy: 0).tap()
        app.keys["more"].tap()
        app.keyboards.keys["1"].tap()
        app.children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .textField).element(boundBy: 1).tap()
        app.keys["more"].tap()
        app.keyboards.keys["0"].tap()
        app.buttons["Calculate BMI"].tap()
        let textField = XCUIApplication().children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .textField).element(boundBy: 2)
        XCTAssertNotEqual(textField.value as? String, "inf")
        
        
        
    }
    
    func testUserInputsTextInWeightAndHeightFields(){
        
        let app = XCUIApplication()
        app.buttons["Let's Go"].tap()
        app.children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .textField).element(boundBy: 0).tap()
        app.keyboards.keys["a"].tap()
        app.children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .textField).element(boundBy: 1).tap()
        app.keyboards.keys["a"].tap()
        app.buttons["Calculate BMI"].tap()
        
        
        
    }
    func testUserPressesCalculateWithoutInputingAnything() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let app = XCUIApplication()
        app.buttons["Let's Go"].tap()
        app.buttons["Calculate BMI"].tap()
                
    }

}
