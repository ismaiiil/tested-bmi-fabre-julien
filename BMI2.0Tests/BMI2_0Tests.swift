//
//  BMI2_0Tests.swift
//  BMI2.0Tests
//
//  Created by Ismail Nassurally on 03/12/2018.
//  Copyright © 2018 Julien Fabre. All rights reserved.
//

import XCTest

@testable import BMI2_0

class BMI2_0Tests: XCTestCase {
    var corelogic:CoreLogic?
    override func setUp() {
        corelogic = CoreLogic()
    }

    override func tearDown() {
        corelogic = nil
    }

    func testCalculate_CorrectBMICalculation() {
        //Arrange
        let weight = 50.0
        let height = 150.0
        //Act
        let expectedResult = weight / (height*height)
        let actualResult = corelogic?.calculate(weight: weight, height: height)
        XCTAssertEqual(expectedResult, actualResult,"The actualResult was \(actualResult ?? 0.0) and the expectedResult was \(expectedResult)")
    }
    
    func testCalculate_UnreasonableCaseHeightIsZeroShouldNotReturnInfinity(){
        //Arrange
        let weight = 50.0
        let height = 0.0
        //Act
        let unwantedResult = weight / (height*height)
        let actualResult = corelogic?.calculate(weight: weight, height: height)
        XCTAssertNotEqual(unwantedResult,actualResult,"The actualResult was \(actualResult ?? 0.0)")
    }

}
